# Change Log

# 2021-06-25-0638

* Added CAnnounce plugin (testing).
* Updated version of ReverseFF (and related plugins).
* Updated version of SourceMod.
* Updated version of MetaMod.
* Added configuration for 0 reversed friendly-fire damage for infected.

# 2021-04-18

* Added takeover and friendly-fire and respawn mods.
* Updated SourceMod and MetaMod.
* Moved the mod download and other support file URLs into setup_env.bsh to make Dockerfile easier to manage.

## Version 5

This is the live version of the configuration.

## Version 4

Merged in from the source files on my server.

## Version 3

	gpg: assuming signed data in 'thatZombieGameSetup_v3.tar'
	gpg: Signature made Sun 19 Jul 2020 15:52:38 AEST
	gpg:                using RSA key 2C3EBF239A279270E5399EF65DE5F96B92FA7361
	gpg: Good signature from "Conor Andrew Buckley" [ultimate]
	gpg:                 aka "[jpeg image of size 5400]" [ultimate]


## Version 2

	gpg: assuming signed data in 'thatZombieGameSetup_v2.tar'
	gpg: Signature made Sun 19 Jul 2020 15:57:37 AEST
	gpg:                using RSA key 2C3EBF239A279270E5399EF65DE5F96B92FA7361
	gpg: Good signature from "Conor Andrew Buckley" [ultimate]
	gpg:                 aka "[jpeg image of size 5400]" [ultimate]

## Version 1

	gpg: assuming signed data in 'thatZombieGameSetup_v1.tar'
	gpg: Signature made Sun 19 Jul 2020 16:01:25 AEST
	gpg:                using RSA key 2C3EBF239A279270E5399EF65DE5F96B92FA7361
	gpg: Good signature from "Conor Andrew Buckley" [ultimate]
	gpg:                 aka "[jpeg image of size 5400]" [ultimate]

Last change according to www.conorab.com: 2019-03-25
